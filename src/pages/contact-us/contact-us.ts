import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {

  contact_data: any = {};
  contactusForm: FormGroup;
  submitAttempt: boolean;
  islogin: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public api: ApiServiceProvider,
    public toastCtrl: ToastController,
    private callNumber: CallNumber
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.contactusForm = formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.email],
      mobno: ['', Validators.required],
      note: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }

  call(num) {
    this.callNumber.callNumber(num, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }


  contactUs() {
    this.submitAttempt = true;
    if (this.contactusForm.valid) {

      this.contact_data = {
        "user": this.islogin._id,
        "email": this.contactusForm.value.mail,
        "msg": this.contactusForm.value.note,
        "phone": (this.contactusForm.value.mobno).toString(),
        
        // "dealerid": this.islogin.email,
        // "dealerName": this.islogin.fn + ' ' + this.islogin.ln
        // "dealerid": "gaurav.gupta@adnatesolutions.com",
        // "dealerName": "Gaurav Gupta"
      }
      this.api.startLoading().present();
      this.api.contactusApi(this.contact_data)
        .subscribe(data => {
          this.api.stopLoading();
          console.log(data.message)
          if(data.message == 'email sent') {
            let toast = this.toastCtrl.create({
              message: 'Your request has been submitted successfully. We will get back to you soon.',
              position: 'bottom',
              duration: 3000
            });
  
            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            //   // this.contactusForm.reset();
            //   this.navCtrl.setRoot(ContactUsPage);
            // });
  
            toast.present();
            this.navCtrl.setRoot(ContactUsPage);
          } else {
            let toast = this.toastCtrl.create({
              message: 'Something went wrong. Please try after some time.',
              position: 'bottom',
              duration: 3000
            });
  
            toast.onDidDismiss(() => {
              console.log('Dismissed toast');
              this.navCtrl.setRoot(ContactUsPage);
            });
  
            toast.present();
          }
          
        },
          error => {
            this.api.stopLoading();
            console.log(error);
          });
    }
  }

}
