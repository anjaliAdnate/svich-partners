import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyReportPage } from './daily-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    DailyReportPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyReportPage),
    SelectSearchableModule
  ],
})
export class DailyReportPageModule {}
