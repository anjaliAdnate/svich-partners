import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaytmwalletloginPage } from './paytmwalletlogin';

@NgModule({
  declarations: [
    PaytmwalletloginPage,
  ],
  imports: [
    IonicPageModule.forChild(PaytmwalletloginPage),
  ],
})
export class PaytmwalletloginPageModule {}
