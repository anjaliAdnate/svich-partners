import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-fuel-consumption-report',
  templateUrl: 'fuel-consumption-report.html',
})
export class FuelConsumptionReportPage {
  islogin: any;
  portstemp: any[] = [];
  datetimeStart: any;
  datetimeEnd: any;
  vehId: any;
  _listData: any[] = [];
  fuelDataArr: any[] = [];
  eventT: any;
  reportType: any[] = [];
  repkey: string = "Time";
  selectedVehicle: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider,
    private modalCtrl: ModalController) {
    // this.reportType = ["Time", "Daily", "Trip"];
    this.reportType = ["Time", "Daily", "Trip"];
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuelConsumptionReportPage');
  }
  ngOnInit() {
    this.getdevices();
  }

  getFuelNotifs(fuelData, key) {
    this.getData(fuelData, key);

  }

  getData(fuelData, key) {
    // this.eventType = key;
    // const paramData = this.navParams.get('paramMaps');
    const _url = "http://login.svich.co.in/gps/getFuelNotifs";
    const payload = {
      "start_time": fuelData.start_time,
      "end_time": fuelData.end_time,
      "imei": fuelData.imei,
      "event": key
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(_url, payload)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log("response: ", resp)
        // this.fuelData = resp;
        this.modalCtrl.create('FuelEventsComponent', {
          paramMaps: resp,
          // event: key
        }).present();
      },
      err=> {
        this.apiCall.stopLoading();
      })
  }

  reportWise(repId) {
    console.log("repId: ", repId);
  }

  getId(veh) {
    this.vehId = veh;
    // this.vehId = veh.Device_ID;
  }

  getdevices() {
    var baseURLp = 'http://login.svich.co.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getReport() {
    var payload = {};
    var _basU = "http://login.svich.co.in/gps/updatedFuelReport";
    debugger
    if (this.repkey === "Daily" || this.repkey === "Trip") {
      if (this.vehId == undefined) {
        let toast = this.toastCtrl.create({
          message: 'Please Select Vehicle..',
          duration: 1500,
          position: "bottom"
        })
        toast.present();
      } else {
        if (this.repkey === "Daily") {
          payload = {
            "from": new Date(this.datetimeStart).toISOString(),
            "to": new Date(this.datetimeEnd).toISOString(),
            "user": this.islogin._id,
            "daywise": true,
            "imei": this.vehId.Device_ID,
            "vehicle": this.vehId._id
          }
        } else if (this.repkey === "Trip") {
          payload = {
            "from": new Date(this.datetimeStart).toISOString(),
            "to": new Date(this.datetimeEnd).toISOString(),
            "user": this.islogin._id,
            "trip": true,
            "imei": this.vehId.Device_ID,
            "vehicle": this.vehId._id
          }
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_basU, payload)
          .subscribe(res => {
            this.apiCall.stopLoading();
            console.log("report data: ", res)
            this.fuelDataArr = [];
            if (res.message === undefined) {
              this.fuelDataArr = res;
            } else {
              let toast = this.toastCtrl.create({
                message: "No data found..",
                duration: 1500,
                position: 'bottom'
              });
              toast.present();
            }
          });
      }
    } else if (this.repkey === "Time") {
      if (this.vehId != undefined) {
        payload = {
          "from": new Date(this.datetimeStart).toISOString(),
          "to": new Date(this.datetimeEnd).toISOString(),
          "imei": this.vehId.Device_ID,
          "vehicle": this.vehId._id
        }
      } else {
        payload = {
          "from": new Date(this.datetimeStart).toISOString(),
          "to": new Date(this.datetimeEnd).toISOString(),
          "user": this.islogin._id
        }
      }
      this.apiCall.startLoading().present();
      this.apiCall.urlpasseswithdata(_basU, payload)
        .subscribe(res => {
          this.apiCall.stopLoading();
          console.log("report data: ", res)
          this.fuelDataArr = [];
          if (res.message === undefined) {
            this.fuelDataArr = res;
          } else {
            let toast = this.toastCtrl.create({
              message: "No data found..",
              duration: 1500,
              position: 'bottom'
            });
            toast.present();
          }
        });
    }

  }

  device_address(device, index) {
    let that = this;
    that.fuelDataArr[index].saddress = "N/A";
    if (!device.start_location) {
      that.fuelDataArr[index].saddress = "N/A";
    } else if (device.start_location) {
      this.geocoderApi.reverseGeocode(Number(device.start_location.lat), Number(device.start_location.lng))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.fuelDataArr[index].saddress = str;
        })
    }
  }

  device_eaddress(device, index) {
    debugger
    let that = this;
    that.fuelDataArr[index].eaddress = "N/A";
    if (!device.end_location) {
      that.fuelDataArr[index].eaddress = "N/A";
    } else if (device.end_location) {
      this.geocoderApi.reverseGeocode(Number(device.end_location.lat), Number(device.end_location.lng))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.fuelDataArr[index].eaddress = str;
        })
    }
  }
  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
    return hDisplay + mDisplay + sDisplay;
  }
}
