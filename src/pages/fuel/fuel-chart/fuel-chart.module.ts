import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelChartPage } from './fuel-chart';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@NgModule({
  declarations: [
    FuelChartPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelChartPage),
    SelectSearchableModule
  ],
  providers: [ScreenOrientation]
})
export class FuelChartPageModule {}
