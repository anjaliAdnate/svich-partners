import { Component, OnInit } from "@angular/core";
import { NavController, NavParams, AlertController, ViewController, ToastController, IonicPage, ActionSheetController, LoadingController, Platform } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";
import * as moment from 'moment';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
declare var cordova: any;
@IonicPage()
@Component({
    selector: 'page-add-customer-model',
    templateUrl: './add-customer-modal.html'
})
export class AddCustomerModal implements OnInit {

    addcustomerform: FormGroup;
    customerdata: any = {};
    Customeradd: any;
    submitAttempt: boolean;
    selectDealer: any;
    isSuperAdminStatus: boolean;
    islogin: any;
    dealerdata: any;
    currentYear: any;
    lastImage: any = null;
    lastImage1: any = null;
    Imgloading: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public apicallCustomer: ApiServiceProvider,
        public alerCtrl: AlertController,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        public actionSheetCtrl: ActionSheetController,
        public file: File,
        public filePath: FilePath,
        public camera: Camera,
        public transferObj: TransferObject,
        public transfer: Transfer,
        public loadingCtrl: LoadingController,
        public platform: Platform
    ) {

        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isSuperAdminStatus = this.islogin.isSuperAdmin
        console.log("isDealer=> " + this.isSuperAdminStatus);

        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);

        this.currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);

        this.addcustomerform = formBuilder.group({
            userId: ['', Validators.required],
            Firstname: ['', Validators.required],
            LastName: ['', Validators.required],
            // emailid: [this.islogin.account, Validators.required],
            contact_num: [''],
            // password: ['', Validators.required],
            // confpassword: [''],
            // address: ['', Validators.required],
            ExipreDate: [this.currentYear, Validators.required],
            dealer_firstname: [''],
            adhar: [''],
            driverL: ['']
        })
    }

    ngOnInit() {
        this.getAllDealers();
    }

    private presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    public presentActionSheet(key) {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, key);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.CAMERA, key);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    }

    public takePicture(sourceType, key) {
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };

        this.camera.getPicture(options).then((imagePath) => {
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), key);
                    });
            } else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), key);
            }
        }, (err) => {
            // this.presentToast('Error while selecting image.');
            console.log("Error while selecting image.", err);
        });
    }

    private copyFileToLocalDir(namePath, currentName, newFileName, key) {
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            if (key === 'adhar') {
                this.lastImage = newFileName;
                this.addcustomerform.patchValue({
                    adhar: this.lastImage,
                });
                this.uploadImage(key);
            }
            if (key === 'license') {

                this.lastImage1 = newFileName;
                this.addcustomerform.patchValue({
                    driverL: this.lastImage1,
                });
                this.uploadImage(key);
            }

        }, error => {
            this.presentToast('Error while storing file.');
        });
    }

    public pathForImage(img) {
        console.log("Image=>", img);
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }

    private createFileName() {
        var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";
        return newFileName;
    }

    public uploadImage(key) {

        var url = this.apicallCustomer.mainUrl + "users/uploadImage";
        if (key === 'adhar') {
            var targetPath = this.pathForImage(this.lastImage);
            var filename = this.lastImage;
        } else if (key === 'license') {
            var targetPath = this.pathForImage(this.lastImage1);
            var filename = this.lastImage1;
        }
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        this.transferObj = this.transfer.create();
        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();
        this.transferObj.upload(targetPath, url, options).then(data => {
            this.Imgloading.dismissAll();
            if (key === 'adhar') {
                this.addcustomerform.patchValue({
                    adhar: data.response
                })
            } else if (key === 'license') {
                this.addcustomerform.patchValue({
                    driverL: data.response
                })
            }
            // this.dlUpdate(data.response);
        }, err => {
            console.log("uploadError=>", err)
            this.lastImage = null;
            this.Imgloading.dismissAll();
            this.presentToast('Error while uploading file, Please try again !!!');
        });
    }

    removeImg(key) {
        if (key === 'adhar') {
            this.lastImage = null;
        } else if (key === 'license') {
            this.lastImage1 = null;
        }

    }

    dealerOnChnage(dealer) {
        console.log(dealer);
        this.dealerdata = dealer;
        console.log("dealer id=> " + this.dealerdata.dealer_id);
    }

    addcustomer() {
        this.submitAttempt = true;
        var custPayload: any = {};
        // console.log(devicedetails);
        if (this.addcustomerform.valid) {

            if (this.islogin.isSuperAdmin == true) {
                // this.customerdata = {
                //     "first_name": this.addcustomerform.value.Firstname,
                //     "last_name": this.addcustomerform.value.LastName,
                //     // "email": this.addcustomerform.value.emailid,
                //     "phone": this.addcustomerform.value.contact_num,
                //     // "password": this.addcustomerform.value.password,
                //     "isDealer": false,
                //     "custumer": true,
                //     "status": true,
                //     "user_id": this.addcustomerform.value.userId,
                //     // "address": this.addcustomerform.value.address,
                //     "supAdmin": this.islogin._id
                // }
                custPayload = {
                    "first_name": this.addcustomerform.value.Firstname,
                    "last_name": this.addcustomerform.value.LastName,
                    // "email": "anjalitest@oneqlik.in",
                    "password": "123",
                    // "password": "Sharvari@123",
                    "phone": this.addcustomerform.value.contact_num,
                    "supAdmin": this.islogin._id,
                    "isDealer": false,
                    "expdate": new Date(this.addcustomerform.value.ExipreDate).toISOString(),
                    // "Dealer": "5d6f25d953514778bf94f99b",
                    "custumer": true,
                    "user_id": this.addcustomerform.value.userId,
                    // "address": "nagpur",
                    "imageDoc": [
                        {
                            "doctype": "Adhar",
                            "image": this.addcustomerform.value.adhar,
                            "phone": ""
                        },
                        {
                            "doctype": "Driver License",
                            "image": this.addcustomerform.value.driverL,
                            "phone": ""
                        }
                    ]
                }
            } else {
                if (this.islogin.isDealer == true) {
                    // this.customerdata = {
                    //     "first_name": this.addcustomerform.value.Firstname,
                    //     "last_name": this.addcustomerform.value.LastName,
                    //     // "email": this.addcustomerform.value.emailid,
                    //     "phone": this.addcustomerform.value.contact_num,
                    //     // "password": this.addcustomerform.value.password,
                    //     "isDealer": this.islogin.isDealer,
                    //     "custumer": true,
                    //     "status": true,
                    //     "user_id": this.addcustomerform.value.userId,
                    //     // "address": this.addcustomerform.value.address,
                    //     "supAdmin": this.islogin.supAdmin,
                    //     // "Dealer": this.islogin.Dealer_ID['_id']
                    // }
                    custPayload = {
                        "first_name": this.addcustomerform.value.Firstname,
                        "last_name": this.addcustomerform.value.LastName,
                        // "email": "anjalitest@oneqlik.in",
                        "password": "123",
                        "phone": this.addcustomerform.value.contact_num,
                        "supAdmin": this.islogin.supAdmin,
                        "isDealer": this.islogin.isDealer,
                        "expdate": new Date(this.addcustomerform.value.ExipreDate).toISOString(),
                        // "Dealer": "5d6f25d953514778bf94f99b",
                        "custumer": true,
                        "user_id": this.addcustomerform.value.userId,
                        // "address": "nagpur",
                        "imageDoc": [
                            {
                                "doctype": "Adhar",
                                "image": this.addcustomerform.value.adhar,
                                "phone": ""
                            },
                            {
                                "doctype": "Driver License",
                                "image": this.addcustomerform.value.driverL,
                                "phone": ""
                            }
                        ]
                    }
                }
            }

            if (this.dealerdata != undefined) {
                // this.customerdata.Dealer = this.dealerdata.dealer_id;
                custPayload.Dealer = this.dealerdata.dealer_id;
            } else {
                // this.customerdata.Dealer = this.islogin._id;
                custPayload.Dealer = this.islogin._id;
            }

            this.apicallCustomer.startLoading().present();
            // this.apicallCustomer.signupApi(this.customerdata)
            this.apicallCustomer.signupApi(custPayload)
                .subscribe(data => {
                    this.apicallCustomer.stopLoading();
                    this.Customeradd = data;

                    console.log("devicesadd=> ", this.Customeradd)
                    let toast = this.toastCtrl.create({
                        message: 'Customer was added successfully',
                        position: 'top',
                        duration: 1500
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss();
                    });

                    toast.present();
                },
                    err => {
                        this.apicallCustomer.stopLoading();
                        var body = err._body;
                        console.log(body)
                        var msg = JSON.parse(body);
                        console.log(msg)
                        var namepass = [];
                        namepass = msg.split(":");
                        var name = namepass[1];

                        let alert = this.alerCtrl.create({
                            title: 'Oops!',
                            message: name,
                            buttons: ['OK']
                        });
                        alert.present();
                        console.log(err);
                    });
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    getAllDealers() {
        console.log("get dealer");

        var baseURLp = 'http://login.svich.co.in/users/getAllDealerVehicles';

        this.apicallCustomer.getAllDealerCall(baseURLp)
            .subscribe(data => {
                this.selectDealer = data;
                console.log(this.selectDealer);
            },
                error => {
                    console.log(error)
                });
    }


}



